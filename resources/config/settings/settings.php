<?php

return [
    'site_title' => [
        'type' => 'anomaly.field_type.editor',
        'config' => [
            'default_value' => 'Arsh<span class="base-color">ia</span>',
        ]
    ],
    'facebook_url' => 'anomaly.field_type.url',
    'twitter_url' => 'anomaly.field_type.url',
    'instagram_url' => 'anomaly.field_type.url',
    'linkedin_url' => 'anomaly.field_type.url',
    'photo' => [
        'type' => 'anomaly.field_type.file',
        'config' => [
            'folders' => ["images"],
            'mode' => 'upload',
        ]
    ],
    'cv' => 'anomaly.field_type.file',
    'age' => [
        'type' => 'anomaly.field_type.datetime',
        'config' => [
            "mode" => "date"
        ]
    ],
    'website' => 'anomaly.field_type.url',
    'phone' => 'anomaly.field_type.text',
    'city' => [
        'type' => 'anomaly.field_type.relationship',
        'config' => [
            "mode"           => "lookup",
            'related' => \Visiosoft\LocationModule\City\CityModel::class,
        ]
    ],
    'degree' => 'anomaly.field_type.text',
    'mail' => 'anomaly.field_type.text',
    'freelance' => [
        'type' => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                "mt.theme.arshia::field.available" => "mt.theme.arshia::field.available",
                "mt.theme.arshia::field.not_available" => "mt.theme.arshia::field.not_available",
            ]
        ]
    ]
];