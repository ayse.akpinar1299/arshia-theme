<?php

return [
    'monitoring' => [
        'stacked' => false,
        'tabs' => [
            'general' => [
                'title' => 'mt.theme.arshia::section.general.name',
                'fields' => [
                    'site_title', 'photo', 'cv', 'facebook_url', 'twitter_url', 'instagram_url', 'linkedin_url',
                ]
            ],
            'about_me' => [
                'title' => 'mt.theme.arshia::section.about_me.name',
                'fields' => [
                    'age', 'website', 'phone', 'city', 'degree', 'mail', 'freelance',
                ]
            ]
        ],
    ],
];
