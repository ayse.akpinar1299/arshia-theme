<?php

return [
    'follow_me' => 'Takip Et',
    'download_cv' => 'CV İndir',
    'birthday' => 'Doğumgünü',
    'website' => 'Website',
    'phone' => 'Telefon',
    'city' => 'Şehir',
    'age' => 'Yaş',
    'degree' => 'Derece',
    'mail' => 'Mail',
    'freelance' => 'Uzaktan',
    'hire_me' => 'İletişime Geç',
    'available' => 'Uygun',
    'not_available' => 'Uygun Değil',
    'show_more' => 'Daha Fazla Göster',
    'view_more' => 'Daha Fazla Görüntüle',
    'read_more' => 'Daha Fazla Oku',
    'send_message' => 'Mesaj Gönder',
    'all' => 'Tümü',
    'categories' => 'Kategoriler',
    'your_name' => 'İsim',
    'your_email' => 'Mail Adresi',
    'message' => 'Mesaj',

    'hero' => [
        'name' => 'Ben <span class="base-color"> Ayşe</span>',
        'elements' => 'Psikolog.',
        'detail' => ''
    ],

    'about' => [
        'about_me' => 'Hakkımda',
        'job_title' => '<span class="base-color">Psikolog',
    ],

    'skill' => [
        'subtitle' => 'Yetenek Dereceleri',
        'title' => 'Yeteneklerim',
    ],

    'service' => [
        'subtitle' => 'Destek',
        'title' => 'Hizmetlerim',
    ],

    'portfolio' => [
        'subtitle' => '',
        'title' => 'Çalışmalarım',
    ],

    'testimonial' => [
        'subtitle' => 'Ne söylediler',
        'title' => 'Kullanıcı Yorumları'
    ],

    'blog' => [
        'subtitle' => 'Tüm Yazılar',
        'title' => 'En Yeni Yazı',
        'blog' => 'Blog',
        'latest_blog' =>'Son Yazı',
        'recent_post' => 'Geçmiş Yazılar',
    ],

     'contact' => [
         'subtitle' => 'İstediğin zaman ulaş',
         'title' => 'İletişime Geç',
         'phone' => 'Ara',
         'address' => 'Ofis',
         'email' => 'Mail At',
     ]
];
