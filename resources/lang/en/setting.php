<?php

return [
    'site_title' => [
        'name' => 'Site Title',
    ],
    'photo' => [
        'name' => 'Photo',
    ],
    'cv' => [
        'name' => 'CV',
    ],
    'facebook_url' => [
        'name' => 'Facebook URL',
    ],
    'twitter_url' => [
        'name' => 'Twitter URL',
    ],
    'instagram_url' => [
        'name' => 'Instagram URL',
    ],
    'linkedin_url' => [
        'name' => 'Linkedin URL',
    ],
    'age' => [
        'name' => 'Age',
    ],
    'website' => [
        'name' => 'Website',
    ],
    'phone' => [
        'name' => 'Phone',
    ],
    'city' => [
        'name' => 'City',
    ],
    'degree' => [
        'name' => 'Degree',
    ],
    'mail' => [
        'name' => 'Mail',
    ],
    'freelance' => [
        'name' => 'Freelance',
    ],
];
