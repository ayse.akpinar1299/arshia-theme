<?php namespace Mt\ArshiaTheme;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Mt\ArshiaTheme\Seed\PageSeeder;
use Mt\ArshiaTheme\Seed\PostSeeder;
use Mt\ArshiaTheme\Seed\RepeaterSeeder;
use Mt\ArshiaTheme\Seed\SettingSeeder;

class ArshiaThemeSeeder extends Seeder
{
    public function run()
    {
        $this->call(RepeaterSeeder::class);

        $this->call(PageSeeder::class);

        $this->call(PostSeeder::class);

        $this->call(SettingSeeder::class);
    }
}