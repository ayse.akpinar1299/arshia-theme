<?php namespace Mt\ArshiaTheme\Seed;

use Anomaly\PostsModule\Type\Contract\TypeRepositoryInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{

    protected $streamRepository;
    protected $fields;
    protected $assignments;
    protected $typeRepository;

    public function __construct(
        StreamRepositoryInterface     $streamRepository,
        FieldRepositoryInterface      $fields,
        AssignmentRepositoryInterface $assignments,
        TypeRepositoryInterface       $typeRepository
    )
    {
        $this->streamRepository = $streamRepository;
        $this->fields = $fields;
        $this->assignments = $assignments;
        $this->typeRepository = $typeRepository;
    }

    protected $post_fields = [
        'image' => [
            'name' => 'Image',
            'namespace' => 'posts',
            'slug' => 'image',
            'type' => 'anomaly.field_type.file',
            'locked' => 0,
            'config' => [
                'folders' => ["images"],
                'mode' => 'upload',
            ]
        ],
    ];

    public function run()
    {
        foreach ($this->post_fields as $slug => $item) {
            if (!$field = $this->fields->findBySlugAndNamespace($slug, $item['namespace'])) {
                $field = $this->fields->create($item);
            }

            if ($type = $this->typeRepository->findBySlug('default')) {
                $stream = $type->getEntryStream();

                if (!$this->assignments->findByStreamAndField($stream, $field)) {
                    $this->assignments->create([
                        'stream' => $stream,
                        'field' => $field
                    ]);
                }
            }
        }
    }
}