<?php namespace Mt\ArshiaTheme\Seed;

use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{

    protected $settingRepository;
    public function __construct(
        SettingRepositoryInterface $settingRepository
    )
    {
        $this->settingRepository = $settingRepository;
    }

    public function run()
    {
        $this->settingRepository->set('streams::standard_theme', 'mt.theme.arshia');
    }
}