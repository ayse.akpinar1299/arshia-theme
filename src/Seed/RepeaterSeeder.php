<?php namespace Mt\ArshiaTheme\Seed;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Anomaly\UsersModule\User\UserModel;
use Illuminate\Database\Seeder;
use Visiosoft\CatsModule\Category\CategoryModel;

class RepeaterSeeder extends Seeder
{

    protected $streamRepository;
    protected $fields;
    protected $assignments;

    public function __construct(
        StreamRepositoryInterface     $streamRepository,
        FieldRepositoryInterface      $fields,
        AssignmentRepositoryInterface $assignments
    )
    {
        $this->streamRepository = $streamRepository;
        $this->fields = $fields;
        $this->assignments = $assignments;
    }

    protected $repeaters = [
        'skill_repeater' => [
            'content' => [
                'name' => 'Skill Repeater',
                'namespace' => 'repeater',
                'prefix' => 'repeater_',
                'slug' => 'skill_repeater',
                'translatable' => true,
            ],
            'fields' => ['text', 'percentage', 'color']
        ],
        'services_repeater' => [
            'content' => [
                'name' => 'Service Repeater',
                'namespace' => 'repeater',
                'prefix' => 'repeater_',
                'slug' => 'services_repeater',
                'translatable' => true,
            ],
            'fields' => ['image', 'text', 'description', 'color']
        ],
        'portfolio_repeater' => [
            'content' => [
                'name' => 'Portfolio Repeater',
                'namespace' => 'repeater',
                'prefix' => 'repeater_',
                'slug' => 'portfolio_repeater',
                'translatable' => true,
            ],
            'fields' => ['image', 'text', 'category']
        ],
        'testimonial_repeater' => [
            'content' => [
                'name' => 'Testimonial Repeater',
                'namespace' => 'repeater',
                'prefix' => 'repeater_',
                'slug' => 'testimonial_repeater',
                'translatable' => true,
            ],
            'fields' => ['user', 'description']
        ]
    ];
    protected $repeater_fields = [
        'text' => [
            'field_attributes' => [
                'name' => 'Text',
                'namespace' => 'repeater',
                'slug' => 'text',
                'type' => 'anomaly.field_type.text',
                'locked' => 0,
                'config' => [],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => true,
            ]
        ],
        'percentage' => [
            'field_attributes' => [
                'name' => 'Percentage',
                'namespace' => 'repeater',
                'slug' => 'percentage',
                'type' => 'anomaly.field_type.slider',
                'locked' => 0,
                'config' => [
                    'min' => 0,
                    'max' => 100,
                    'step' => 1,
                ],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => false,
            ]
        ],
        'color' => [
            'field_attributes' => [
                'name' => 'Color',
                'namespace' => 'repeater',
                'slug' => 'color',
                'type' => 'anomaly.field_type.colorpicker',
                'locked' => 0,
                'config' => [],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => false,
            ]
        ],
        'description' => [
            'field_attributes' => [
                'name' => 'Description',
                'namespace' => 'repeater',
                'slug' => 'description',
                'type' => 'anomaly.field_type.textarea',
                'locked' => 0,
                'config' => [],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => true,
            ]
        ],
        'image' => [
            'field_attributes' => [
                'name' => 'Image',
                'namespace' => 'repeater',
                'slug' => 'image',
                'type' => 'anomaly.field_type.file',
                'locked' => 0,
                'config' => [
                    'folders' => ['images'],
                    'mode' => 'upload',
                ],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => false,
            ],
        ],
        'category' => [
            'field_attributes' => [
                'name' => 'Category',
                'namespace' => 'repeater',
                'slug' => 'category',
                'type' => 'anomaly.field_type.relationship',
                'locked' => 0,
                'config' => [
                    'related' => CategoryModel::class,
                ],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => false,
            ],
        ],
        'user' => [
            'field_attributes' => [
                'name' => 'User',
                'namespace' => 'repeater',
                'slug' => 'user',
                'type' => 'anomaly.field_type.relationship',
                'locked' => 0,
                'config' => [
                    'related' => UserModel::class,
                ],
            ],
            'assignment_attributes' => [
                'required' => true,
                'translatable' => false,
            ],
        ],
    ];
    public function run()
    {
        foreach ($this->repeaters as $slug => $arr) {
            if (!$repeater = $this->streamRepository->findBySlugAndNamespace($slug, 'repeater')) {
                $repeater = $this->streamRepository->create($arr['content']);
            }

            foreach ($this->repeater_fields as $field_slug => $field_repeater) {
                if (!$field = $this->fields->findBySlugAndNamespace($field_slug, 'repeater')) {
                    $field = $this->fields->create($field_repeater['field_attributes']);
                }

                if (!$this->assignments->findByStreamAndField($repeater, $field) && in_array($field->getSlug(), $arr['fields'], true)) {
                    $this->assignments->create(
                        array_merge([
                            'stream' => $repeater,
                            'field' => $field,
                        ], $field_repeater['assignment_attributes'])
                    );
                }
            }
        }
    }
}