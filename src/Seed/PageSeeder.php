<?php namespace Mt\ArshiaTheme\Seed;

use Anomaly\PagesModule\Page\Contract\PageRepositoryInterface;
use Anomaly\PagesModule\Type\Contract\TypeRepositoryInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterPortfolioRepeaterEntryModel;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterServicesRepeaterEntryModel;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterSkillRepeaterEntryModel;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterTestimonialRepeaterEntryModel;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{

    protected $streamRepository;
    protected $fields;
    protected $assignments;
    protected $typeRepository;
    protected $pageRepository;

    public function __construct(
        StreamRepositoryInterface     $streamRepository,
        FieldRepositoryInterface      $fields,
        AssignmentRepositoryInterface $assignments,
        PageRepositoryInterface       $pageRepository,
        TypeRepositoryInterface       $typeRepository
    )
    {
        $this->streamRepository = $streamRepository;
        $this->fields = $fields;
        $this->assignments = $assignments;
        $this->typeRepository = $typeRepository;
        $this->pageRepository = $pageRepository;
    }

    protected $page_fields = [
        'skill_repeater_field' => [
            'name' => 'Skill Repeater Field',
            'namespace' => 'pages',
            'slug' => 'skill_repeater_field',
            'type' => 'anomaly.field_type.repeater',
            'locked' => 0,
            'config' => [
                'related' => RepeaterSkillRepeaterEntryModel::class,
            ],
        ],
        'services_repeater_field' => [
            'name' => 'Service Repeater Field',
            'namespace' => 'pages',
            'slug' => 'services_repeater_field',
            'type' => 'anomaly.field_type.repeater',
            'locked' => 0,
            'config' => [
                'related' => RepeaterServicesRepeaterEntryModel::class,
            ]
        ],
        'portfolio_repeater_field' => [
            'name' => 'Portfolio Repeater Field',
            'namespace' => 'pages',
            'slug' => 'portfolio_repeater_field',
            'type' => 'anomaly.field_type.repeater',
            'locked' => 0,
            'config' => [
                'related' => RepeaterPortfolioRepeaterEntryModel::class,
            ]
        ],
        'testimonial_repeater_field' => [
            'name' => 'Testimonial Repeater Field',
            'namespace' => 'pages',
            'slug' => 'testimonial_repeater_field',
            'type' => 'anomaly.field_type.repeater',
            'locked' => 0,
            'config' => [
                'related' => RepeaterTestimonialRepeaterEntryModel::class,
            ]
        ]
    ];

    public function run()
    {
        foreach ($this->page_fields as $slug => $item) {
            if (!$field = $this->fields->findBySlugAndNamespace($slug, $item['namespace'])) {
                $field = $this->fields->create($item);
            }

            if ($type = $this->typeRepository->findBySlug('default')) {
                $stream = $type->getEntryStream();

                if (!$this->assignments->findByStreamAndField($stream, $field)) {
                    $this->assignments->create([
                        'stream' => $stream,
                        'field' => $field
                    ]);
                }
            }
        }

        foreach ($this->pageRepository->all() as $page) {
            $page->update(['visible' => false]);
        }
    }
}